import React from 'react';
import { NavLink, Switch, Route } from 'react-router-dom';
import {Home, Contact, About} from './Page';

const Main = () => (
    <Switch>
      <Route exact path='/' component={Home}></Route>
      <Route exact path='/about' component={About}></Route>
      <Route exact path='/contact' component={Contact}></Route>
    </Switch>
  );

  export default Main;